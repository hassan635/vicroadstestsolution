﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using VicRoadsTestSolution.Src.Helpers;

namespace VicRoadsTestSolution
{
    [Binding]
    public sealed class Hooks
    {

        [AfterScenario]
        public void AfterScenario()
        {
            DriverFactory.NukeDriver();
        }
    }
}
