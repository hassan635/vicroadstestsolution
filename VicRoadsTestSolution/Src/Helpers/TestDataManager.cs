﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Reflection;

namespace VicRoadsTestSolution.Src.Helpers
{

    public static class TestDataManager
    {
        public static string GetTestInputValue(string jsonPathExpression)
        {
            try
            {
                string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"TestData.json");
                string testInputs = File.ReadAllText(path);
                JObject parsedInputs = JObject.Parse(testInputs);
                return parsedInputs.SelectToken(jsonPathExpression).ToString();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
