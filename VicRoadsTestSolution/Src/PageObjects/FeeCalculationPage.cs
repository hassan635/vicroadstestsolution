﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using VicRoadsTestSolution.Src.Helpers;

namespace VicRoadsTestSolution.Src.PageObjects
{
    public class FeeCalculationPage
    {

        private IWebDriver _driver = null;

        public FeeCalculationPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public void GotoPage()
        {
            try
            {
                _driver.Navigate().GoToUrl("https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void SelectVehicleType(string option)
        {
            try
            {
                var vehicleListElement = _driver.FindElement(By.CssSelector("select[id*=\"VehicleType\"]"), 5);
                var vehicleSelectList = new SelectElement(vehicleListElement);

                vehicleSelectList.SelectByText(option);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void SelectSubType(string option)
        {
            try
            {
                var vehicleListElement = _driver.FindElement(By.CssSelector("select[id*=\"SubType\"]"), 5);
                var vehicleSelectList = new SelectElement(vehicleListElement);

                vehicleSelectList.SelectByText(option);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void InputGarageAddress(string address)
        {
            try
            {
                var inputField = _driver
                        .FindElement(
                        By.CssSelector("input[id*=\"AddressLine_SingleLine_CtrlHolderDivShown\"]"), 5);
                inputField.SendKeys(address);
                inputField.SendKeys(Keys.Tab);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void SelectPermitDuration(string option)
        {
            try
            {
                var vehicleListElement = _driver.FindElement(By.CssSelector("select[id*=\"PermitDuration\"]"), 5);
                var vehicleSelectList = new SelectElement(vehicleListElement);

                vehicleSelectList.SelectByText(option);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void ClickCalculateButton()
        {
            try
            {
                _driver.FindElement(By.CssSelector("a[id*=\"panel_btnCal\"]"), 5).Click();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetFeeCalculation()
        {
            try
            {
                string result = string.Empty;
                while (result == string.Empty)
                {
                    result = _driver.FindElement(By.CssSelector("span[id*=\"spnTotalPermit\"]"), 5).Text;
                    System.Threading.Thread.Sleep(1);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PermitTypeSelectionPage GotoNextStep()
        {
            try
            {
                var nextStepBtn = _driver.FindElement(By.CssSelector("input[id*=\"btnNext\"]"), 10);

                var executor = (IJavaScriptExecutor)_driver;
                executor.ExecuteScript("arguments[0].click();", nextStepBtn);
                return new PermitTypeSelectionPage(_driver);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
