﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using VicRoadsTestSolution.Src.Helpers;

namespace VicRoadsTestSolution.Src.PageObjects
{
    public class PermitTypeSelectionPage
    {

        private IWebDriver _driver = null;

        public PermitTypeSelectionPage(IWebDriver driver)
        {
            _driver = driver;
        }


        public string GetPageHeadingText()
        {
            try
            {
                return _driver
                        .FindElement(
                        By.ClassName("progress-bar-title"), 5).Text;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
