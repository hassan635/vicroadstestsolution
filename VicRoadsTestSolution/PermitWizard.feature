﻿Feature: PermitWizard
	In order to show permit type page heading
	As an end user of VicRoads
	I want to verify it's correctness

Scenario: Verify Permit Type Page Heading
	Given I am on the unregistered car permit fee page
    And I have entered my vehical type
    And I have entered my sub type
    And I have entered my garage location
    And I have entered my desired permit duration
	When I choose to calculate
	Then the result should be displayed
    And I should be taken to permit type selection page
	And the page heading is 'Step 2 of 7 : Select permit type'