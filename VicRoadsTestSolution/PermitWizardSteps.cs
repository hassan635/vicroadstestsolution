﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using VicRoadsTestSolution.Src.Helpers;
using VicRoadsTestSolution.Src.PageObjects;

namespace VicRoadsTestSolution
{
    [Binding]
    public sealed class PermitWizardSteps
    {

        FeeCalculationPage feeCalculationPage = null;
        PermitTypeSelectionPage permitTypeSelectionPage = null;

        [Given(@"I am on the unregistered car permit fee page")]
        public void GivenIAmOnTheUnregisteredCarPermitFeePage()
        {
            feeCalculationPage = new FeeCalculationPage(DriverFactory.GetDriver());
            feeCalculationPage.GotoPage();
        }

        [Given(@"I have entered my vehical type")]
        public void GivenIHaveEnteredMyVehicalType()
        {
            feeCalculationPage.SelectVehicleType(TestDataManager.GetTestInputValue("$..vehicle_type"));
        }

        [Given(@"I have entered my sub type")]
        public void GivenIHaveEnteredMySubType()
        {
            feeCalculationPage.SelectSubType(TestDataManager.GetTestInputValue("$..sub_type"));
        }

        [Given(@"I have entered my garage location")]
        public void GivenIHaveEnteredMyGarageLocation()
        {

            feeCalculationPage.InputGarageAddress(TestDataManager.GetTestInputValue("$..garage_address"));
        }


        [Given(@"I have entered my desired permit duration")]
        public void GivenIHaveEnteredMyDesiredPermitDuration()
        {

            feeCalculationPage.SelectPermitDuration(TestDataManager.GetTestInputValue("$..desired_permit_duration"));
        }


        [When(@"I choose to calculate")]
        public void WhenIChooseToCalculate()
        {
            feeCalculationPage.ClickCalculateButton();
        }

        [Then(@"the result should be displayed")]
        public void TheResultShouldBeDisplayed()
        {
            Assert.AreEqual("$45.40", feeCalculationPage.GetFeeCalculation());
        }


        [Then(@"I should be taken to permit type selection page")]
        public void TakenToPermitSelectionPage()
        {
            permitTypeSelectionPage = feeCalculationPage.GotoNextStep();
        }

        [Then(@"the page heading is '(.*)'")]
        public void VerifyPermitTypePageHeading(string expectedHeading)
        {
            //Final assertion for verifying heading for Permit type page
            Assert.AreEqual(expectedHeading, permitTypeSelectionPage.GetPageHeadingText());
        }

    }
}
